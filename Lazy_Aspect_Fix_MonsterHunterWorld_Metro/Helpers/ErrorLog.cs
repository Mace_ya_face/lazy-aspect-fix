﻿using System;
using System.IO;


namespace Lazy_Aspect_Fix_MonsterHunterWorld_Metro.Helpers
{
    class ErrorLog
    {
        public static void Log(string error, string stack)
        {
            var logFile = File.AppendText("./lazy_error.log");

            logFile.WriteLine(DateTime.Now + " | " + error + " | Stack trace: " + stack);
            logFile.Flush();
            logFile.Close();
            logFile.Dispose();
        }
    }
}
