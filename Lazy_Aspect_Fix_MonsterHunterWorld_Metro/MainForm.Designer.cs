﻿/*
    This file is part of Lazy Aspect Fix.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Lazy Aspect Fix is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Lazy Aspect Fix.  If not, see <https://www.gnu.org/licenses/>. 
*/

namespace Mace_Lazy_Aspect_fix
{
    partial class mainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(mainForm));
            this.userXRes = new MetroFramework.Controls.MetroTextBox();
            this.patchButton = new MetroFramework.Controls.MetroButton();
            this.userYRes = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.lazyProgressSpinner = new MetroFramework.Controls.MetroProgressSpinner();
            this.lazyOutput = new MetroFramework.Controls.MetroLabel();
            this.SuspendLayout();
            // 
            // userXRes
            // 
            // 
            // 
            // 
            this.userXRes.CustomButton.Image = null;
            this.userXRes.CustomButton.Location = new System.Drawing.Point(95, 1);
            this.userXRes.CustomButton.Name = "";
            this.userXRes.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.userXRes.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.userXRes.CustomButton.TabIndex = 1;
            this.userXRes.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.userXRes.CustomButton.UseSelectable = true;
            this.userXRes.CustomButton.Visible = false;
            this.userXRes.Lines = new string[0];
            this.userXRes.Location = new System.Drawing.Point(41, 97);
            this.userXRes.MaxLength = 32767;
            this.userXRes.Name = "userXRes";
            this.userXRes.PasswordChar = '\0';
            this.userXRes.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.userXRes.SelectedText = "";
            this.userXRes.SelectionLength = 0;
            this.userXRes.SelectionStart = 0;
            this.userXRes.ShortcutsEnabled = true;
            this.userXRes.Size = new System.Drawing.Size(117, 23);
            this.userXRes.Style = MetroFramework.MetroColorStyle.Purple;
            this.userXRes.TabIndex = 0;
            this.userXRes.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.userXRes.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.userXRes.UseSelectable = true;
            this.userXRes.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.userXRes.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // patchButton
            // 
            this.patchButton.Highlight = true;
            this.patchButton.Location = new System.Drawing.Point(321, 97);
            this.patchButton.Name = "patchButton";
            this.patchButton.Size = new System.Drawing.Size(75, 23);
            this.patchButton.Style = MetroFramework.MetroColorStyle.Purple;
            this.patchButton.TabIndex = 2;
            this.patchButton.Text = "Patch";
            this.patchButton.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.patchButton.UseSelectable = true;
            this.patchButton.Click += new System.EventHandler(this.PatchButton_Click_1);
            // 
            // userYRes
            // 
            // 
            // 
            // 
            this.userYRes.CustomButton.Image = null;
            this.userYRes.CustomButton.Location = new System.Drawing.Point(95, 1);
            this.userYRes.CustomButton.Name = "";
            this.userYRes.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.userYRes.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.userYRes.CustomButton.TabIndex = 1;
            this.userYRes.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.userYRes.CustomButton.UseSelectable = true;
            this.userYRes.CustomButton.Visible = false;
            this.userYRes.Lines = new string[0];
            this.userYRes.Location = new System.Drawing.Point(182, 97);
            this.userYRes.MaxLength = 32767;
            this.userYRes.Name = "userYRes";
            this.userYRes.PasswordChar = '\0';
            this.userYRes.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.userYRes.SelectedText = "";
            this.userYRes.SelectionLength = 0;
            this.userYRes.SelectionStart = 0;
            this.userYRes.ShortcutsEnabled = true;
            this.userYRes.Size = new System.Drawing.Size(117, 23);
            this.userYRes.Style = MetroFramework.MetroColorStyle.Purple;
            this.userYRes.TabIndex = 3;
            this.userYRes.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.userYRes.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.userYRes.UseSelectable = true;
            this.userYRes.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.userYRes.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(159, 101);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(17, 19);
            this.metroLabel1.Style = MetroFramework.MetroColorStyle.Purple;
            this.metroLabel1.TabIndex = 4;
            this.metroLabel1.Text = "X";
            this.metroLabel1.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // lazyProgressSpinner
            // 
            this.lazyProgressSpinner.Location = new System.Drawing.Point(391, 126);
            this.lazyProgressSpinner.Maximum = 100;
            this.lazyProgressSpinner.Name = "lazyProgressSpinner";
            this.lazyProgressSpinner.Size = new System.Drawing.Size(40, 39);
            this.lazyProgressSpinner.Style = MetroFramework.MetroColorStyle.Purple;
            this.lazyProgressSpinner.TabIndex = 5;
            this.lazyProgressSpinner.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.lazyProgressSpinner.UseSelectable = true;
            this.lazyProgressSpinner.Visible = false;
            // 
            // lazyOutput
            // 
            this.lazyOutput.AutoSize = true;
            this.lazyOutput.Location = new System.Drawing.Point(41, 136);
            this.lazyOutput.Name = "lazyOutput";
            this.lazyOutput.Size = new System.Drawing.Size(0, 0);
            this.lazyOutput.TabIndex = 6;
            this.lazyOutput.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(444, 178);
            this.Controls.Add(this.lazyOutput);
            this.Controls.Add(this.lazyProgressSpinner);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.userYRes);
            this.Controls.Add(this.patchButton);
            this.Controls.Add(this.userXRes);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "mainForm";
            this.Resizable = false;
            this.Style = MetroFramework.MetroColorStyle.Purple;
            this.Text = "Lazy Aspect Fix: Monster Hunter World";
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroTextBox userXRes;
        private MetroFramework.Controls.MetroButton patchButton;
        private MetroFramework.Controls.MetroTextBox userYRes;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroProgressSpinner lazyProgressSpinner;
        private MetroFramework.Controls.MetroLabel lazyOutput;
    }
}

