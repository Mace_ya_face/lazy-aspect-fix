﻿namespace Lazy_Aspect_Fix_MonsterHunterWorld_Metro
{
    public class AddressRange
    {
        public ulong Start { get; private set; }
        public ulong End { get; private set; }

        public AddressRange(ulong start, ulong end)
        {
            Start = start;
            End = end;
        }
    }
}
