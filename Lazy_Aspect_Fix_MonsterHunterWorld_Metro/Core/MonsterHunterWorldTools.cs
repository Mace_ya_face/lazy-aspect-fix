﻿using Lazy_Aspect_Fix_MonsterHunterWorld_Metro.Core;
using Lazy_Aspect_Fix_MonsterHunterWorld_Metro.Helpers;
using System;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;

namespace Lazy_Aspect_Fix_MonsterHunterWorld_Metro
{
    class MonsterHunterWorldTools
    {
        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern int VirtualQueryEx(IntPtr hProcess, IntPtr lpAddress, out WindowsMemoryHelper.MEMORY_BASIC_INFORMATION64 lpBuffer, uint dwLength);
        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern bool ReadProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress, [Out] byte[] lpBuffer, int dwSize, out int lpNumberOfBytesRead);
        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern bool WriteProcessMemory(IntPtr hProcess, ulong lpBaseAddress, byte[] lpBuffer, int dwSize, out int lpNumberOfBytesWritten);

        public static bool TestLastGoodAddress(Process targetProcess, ulong baseAddress, BytePattern SearchTarget)
        {
            byte[] testBytes = new byte[SearchTarget.Bytes.Length];
            ReadProcessMemory(targetProcess.Handle, (IntPtr)baseAddress, testBytes, SearchTarget.Bytes.Length, out int bytesRead);

            if(testBytes.SequenceEqual(SearchTarget.Bytes)) { return true; }

            return false;
        }

        public static byte[] GenerateSearchTarget(GraphicsOptionsIni graphicsOptions)
        {
            float gameOptionsAspectRatio = 2.37f;
            if (graphicsOptions.AspectRatio == "Off") { gameOptionsAspectRatio = 1.777778f; }

            byte[] hexBlackBarOffsets = new byte[8];
            byte[] hexWindowSize = new byte[8];
            byte[] hexRenderResolution = new byte[8];

            if (graphicsOptions.ResX / graphicsOptions.ResY >= 1.777778f)
            {
                var horizontalRenderRes = Convert.ToInt32(Math.Round(graphicsOptions.ResY * gameOptionsAspectRatio));
                var horBlackBarOffset = (graphicsOptions.ResX - (float)horizontalRenderRes) / 2f;
                var horizontalWindowSize = Convert.ToInt32(Math.Floor(graphicsOptions.ResX - horBlackBarOffset));

                var hexHorizontalRenderRes = BitConverter.GetBytes(horizontalRenderRes);
                var hexHorBlackBarOffset = BitConverter.GetBytes(Convert.ToInt32(Math.Floor(horBlackBarOffset)));
                var hexHorizontalWindowSize = BitConverter.GetBytes(horizontalWindowSize);
                var hexVerticalWindowRenderRes = BitConverter.GetBytes(graphicsOptions.ResY);

                Buffer.BlockCopy(hexHorBlackBarOffset, 0, hexBlackBarOffsets, 0, hexHorBlackBarOffset.Length);
                Buffer.BlockCopy(hexHorizontalWindowSize, 0, hexWindowSize, 0, hexHorizontalWindowSize.Length);
                Buffer.BlockCopy(hexVerticalWindowRenderRes, 0, hexWindowSize, 4, hexVerticalWindowRenderRes.Length);
                Buffer.BlockCopy(hexHorizontalRenderRes, 0, hexRenderResolution, 0, hexHorizontalRenderRes.Length);
                Buffer.BlockCopy(hexVerticalWindowRenderRes, 0, hexRenderResolution, 4, hexVerticalWindowRenderRes.Length);
            }
            else
            {
                var verticalRenderRes = Convert.ToInt32(Math.Round(graphicsOptions.ResX / gameOptionsAspectRatio));
                var vertBlackBarOffset = (graphicsOptions.ResY - (float)verticalRenderRes) / 2f;
                var vertWindowSize = Convert.ToInt32(Math.Floor(graphicsOptions.ResY - vertBlackBarOffset));
                
                var hexVertBlackBarOffset = BitConverter.GetBytes(Convert.ToInt32(Math.Floor(vertBlackBarOffset)));
                var hexVertRenderRes = BitConverter.GetBytes(verticalRenderRes);
                var hexHorWindowRenderRes = BitConverter.GetBytes(graphicsOptions.ResX);
                var hexVertWindowSize = BitConverter.GetBytes(vertWindowSize);

                Buffer.BlockCopy(hexVertBlackBarOffset, 0, hexBlackBarOffsets, 4, hexVertBlackBarOffset.Length);
                Buffer.BlockCopy(hexHorWindowRenderRes, 0, hexWindowSize, 0, hexHorWindowRenderRes.Length);
                Buffer.BlockCopy(hexVertWindowSize, 0, hexWindowSize, 4, hexVertWindowSize.Length);
                Buffer.BlockCopy(hexHorWindowRenderRes, 0, hexRenderResolution, 0, hexHorWindowRenderRes.Length);
                Buffer.BlockCopy(hexVertRenderRes, 0, hexRenderResolution, 4, hexVertRenderRes.Length);
            }

            byte[] searchTarget = new byte[24];
            Buffer.BlockCopy(hexBlackBarOffsets, 0, searchTarget, 0, hexBlackBarOffsets.Length);
            Buffer.BlockCopy(hexWindowSize, 0, searchTarget, 8, hexWindowSize.Length);
            Buffer.BlockCopy(hexRenderResolution, 0, searchTarget, 16, hexRenderResolution.Length);
            
            return (searchTarget);
        }

        public static ulong GetAddressByteArray(Process targetProcess, BytePattern searchTarget, ulong startRange, ulong endRange)
        {
            ulong currentPointer = startRange;
            int bytesRead;

            while (currentPointer < endRange)
            {
                WindowsMemoryHelper.MEMORY_BASIC_INFORMATION64 memoryRegion;

                var structByteCount = VirtualQueryEx(targetProcess.Handle, (IntPtr)currentPointer, out memoryRegion, (uint)Marshal.SizeOf(typeof(WindowsMemoryHelper.MEMORY_BASIC_INFORMATION64)));

                if (structByteCount > 0
                    && memoryRegion.RegionSize > 0
                    && memoryRegion.State == (uint)WindowsMemoryHelper.RegionPageState.MEM_COMMIT
                    && WindowsMemoryHelper.CheckProtection(searchTarget, memoryRegion.Protect))
                {
                    var regionStartAddress = memoryRegion.BaseAddress;
                    if (startRange > regionStartAddress)
                    {
                        regionStartAddress = startRange;
                    }

                    var regionEndAddress = memoryRegion.BaseAddress + memoryRegion.RegionSize;
                    if (endRange < regionEndAddress)
                    {
                        regionEndAddress = endRange;
                    }

                    ulong regionBytesToRead = regionEndAddress - regionStartAddress;
                    byte[] regionBytes = new byte[regionBytesToRead];

                    ReadProcessMemory(targetProcess.Handle, (IntPtr)regionStartAddress, regionBytes, regionBytes.Length, out bytesRead);

                    var locateResult = Locate(regionBytes, searchTarget.Bytes);

                    if (locateResult != -1)
                    {
                        return regionStartAddress + (ulong)locateResult;
                    }
                }
                if(memoryRegion.RegionSize != 0)
                {
                    currentPointer = memoryRegion.BaseAddress + memoryRegion.RegionSize;
                }
                else
                {
                    currentPointer += 1;
                }
            }

            return (0);
        }

        public static byte[][] GenerateNewResolutionArrays(byte[] resX, byte[] resY)
        {
            byte[] padding = { 0x00, 0x00, 0x00, 0x00 };
            byte[] primaryResolutionArray = new byte[24];
            byte[] secondaryResolutionArray = new byte[8];

            Buffer.BlockCopy(padding, 0, primaryResolutionArray, 0, padding.Length);
            Buffer.BlockCopy(padding, 0, primaryResolutionArray, 4, padding.Length);
            Buffer.BlockCopy(resX, 0, primaryResolutionArray, 8, resX.Length);
            Buffer.BlockCopy(resY, 0, primaryResolutionArray, 12, resY.Length);
            Buffer.BlockCopy(resX, 0, primaryResolutionArray, 16, resX.Length);
            Buffer.BlockCopy(resY, 0, primaryResolutionArray, 20, resY.Length);

            Buffer.BlockCopy(resX, 0, secondaryResolutionArray, 0, resX.Length);
            Buffer.BlockCopy(resY, 0, secondaryResolutionArray, 4, resY.Length);

            byte[][] newResolutionArrays = new byte[][]
            {
                primaryResolutionArray,
                secondaryResolutionArray
            };

            return (newResolutionArrays);
        }

        public static byte[] GenerateUserMasterOffset(float aspectRatio, float userSlider)
        {
            aspectRatio = aspectRatio * 1080;
            aspectRatio = aspectRatio - 1920;
            aspectRatio = aspectRatio / 2;
            aspectRatio = aspectRatio * userSlider;

            var newHexMasterOffset = BitConverter.GetBytes(aspectRatio);

            return (newHexMasterOffset);
        }

        public static void WriteNewResolution(Process targetProcess, ulong baseAddress, byte[][] newResolutionArray)
        {
            int bytesWritten;

            //Write new resolution values
            if (!WriteProcessMemory(targetProcess.Handle, baseAddress, newResolutionArray[0], newResolutionArray[0].Length, out bytesWritten))
            { throw new Exception("Failed to write new resolution. Address: " + baseAddress); }

            //Write nsecondary resolution values
            if (!WriteProcessMemory(targetProcess.Handle, baseAddress + 0x23D90, newResolutionArray[1], newResolutionArray[1].Length, out bytesWritten))
            { throw new Exception("Failed to write new resolution. Address: " + baseAddress); }

            //Break target coords to force rebuild to fix shaders
            if (!WriteProcessMemory(targetProcess.Handle, baseAddress + 0x78, BitConverter.GetBytes(1), 1, out bytesWritten))
            { throw new Exception("Failed to reset render targets. Address: " + baseAddress); }
        }

        public static void WriteSingleArray(Process targetProcess, ulong baseAddress, byte[] newArray)
        {
            int bytesWritten;

            //Write new array to address
            if (!WriteProcessMemory(targetProcess.Handle, baseAddress, newArray, newArray.Length, out bytesWritten))
            { throw new Exception("Failed to write new array. Address: " + baseAddress); }
        }

        private static int Locate(byte[] self, byte[] candidate)
        {
            if (IsEmptyLocate(self, candidate))
                return -1;

            for (int i = 0; i < self.Length; i++)
            {
                if (!IsMatch(self, i, candidate))
                    continue;

                return (i);
            }

            return -1;
        }

        private static bool IsMatch(byte[] array, int position, byte[] candidate)
        {
            if (candidate.Length > (array.Length - position))
                return false;

            for (int i = 0; i < candidate.Length; i++)
                if (array[position + i] != candidate[i])
                    return false;

            return true;
        }

        private static bool IsEmptyLocate(byte[] array, byte[] candidate)
        {
            return array == null
                || candidate == null
                || array.Length == 0
                || candidate.Length == 0
                || candidate.Length > array.Length;
        }

        public static void ClosePatcher()
        {
            if (Application.MessageLoop)
            {
                Application.Exit();
            }
            else
            {
                Environment.Exit(1);
            }
        }
    }
}
