﻿using Lazy_Aspect_Fix_MonsterHunterWorld_Metro.Helpers;
using MadMilkman.Ini;
using System;
using System.IO;
using System.Windows.Forms;

namespace Lazy_Aspect_Fix_MonsterHunterWorld_Metro.Core
{
    class GraphicsOptionsIni
    {
        public string AspectRatio { get; }
        public int ResX { get; }
        public int ResY { get; }
        public string DLSS { get; }
        public string D3D12 { get; }
        public int UltraWideModeLayout { get; }

        public GraphicsOptionsIni(string path)
        {
            var graphicsOptionsIni = LoadIni(path);

            var graphicsOptionsRes = graphicsOptionsIni.Sections["GraphicsOption"].Keys["Resolution"].Value;
            var splitGraphicsOptionsRes = graphicsOptionsRes.Split('x');
            ResX = Convert.ToInt32(splitGraphicsOptionsRes[0]);
            ResY = Convert.ToInt32(splitGraphicsOptionsRes[1]);
            try
            {
                AspectRatio = graphicsOptionsIni.Sections["GraphicsOption"].Keys["Aspect Ratio"].Value;
            }
            catch
            {
                MessageBox.Show("ERROR: graphics_option.ini does not have an option for aspect ratio. Please toggle the aspect ratio setting in-game and restart Lazy Aspect Fix.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                ErrorLog.Log("Aspect Ratio setting not found." + path, Environment.StackTrace);
                throw new Exception();
            }
            try
            {
                UltraWideModeLayout = Convert.ToInt32(graphicsOptionsIni.Sections["GraphicsOption"].Keys["Ultrawide Mode UI Layout"].Value);
            }
            catch
            {
                MessageBox.Show("ERROR: graphics_option.ini does not have an option for UI layout. Please toggle the aspect ratio setting in-game and restart Lazy Aspect Fix.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                ErrorLog.Log("UI layout setting not found." + path, Environment.StackTrace);
                throw new Exception();
            }
            try
            {
                DLSS = graphicsOptionsIni.Sections["GraphicsOption"].Keys["NVIDIA DLSS"].Value;
            }
            catch
            {
                DLSS = "Off";
            }
            try
            {
                D3D12 = graphicsOptionsIni.Sections["GraphicsOption"].Keys["DirectX12Enable"].Value;
            }
            catch
            {
                D3D12 = "Off";
            }
        }

        public static IniFile LoadIni(string path)
        {
            if (!File.Exists(path))
            {
                MessageBox.Show("ERROR: Could not find " + path, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                ErrorLog.Log("File not found: " + path, Environment.StackTrace);
                throw new FileNotFoundException();
            }

            var iniFile = new IniFile();
            iniFile.Load(path);
            return iniFile;
        }
    }
}
