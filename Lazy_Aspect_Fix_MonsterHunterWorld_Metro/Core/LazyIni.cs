﻿using Lazy_Aspect_Fix_MonsterHunterWorld_Metro.Helpers;
using MadMilkman.Ini;
using System;
using System.IO;
using System.Windows;
using System.Windows.Forms;

namespace Lazy_Aspect_Fix_MonsterHunterWorld_Metro.Core
{
    class LazyIni
    {
        public int IniVer { get; }
        public int ResX { get; set; }
        public int ResY { get; set; }
        public bool CloseOnPatch { get; set; }
        public bool OpenGameOnPatch { get; set; }
        public string ExecutableName { get; set; }
        public string GraphicsOptionsIniLoc { get; set; }
        public ulong AddressRangeStart { get; set; }
        public ulong AddressRangeEnd { get; set; }
        public ulong HUDAddressRangeStart { get; set; }
        public ulong HUDAddressRangeEnd { get; set; }

        public LazyIni(string path)
        {
            if (!File.Exists(path))
            {
                CreateLazyIni();
            }
            var lazyIni = LoadIni(path);

            IniVer = Convert.ToInt32(lazyIni.Sections["Version"].Keys["IniVersion"].Value);
            ResX = Convert.ToInt32(lazyIni.Sections["Resolution"].Keys["resX"].Value);
            ResY = Convert.ToInt32(lazyIni.Sections["Resolution"].Keys["resY"].Value);
            CloseOnPatch = Convert.ToBoolean(lazyIni.Sections["Settings"].Keys["closeOnPatch"].Value);
            OpenGameOnPatch = Convert.ToBoolean(lazyIni.Sections["Experimental"].Keys["openGameOnPatch"].Value);
            ExecutableName = lazyIni.Sections["Settings"].Keys["executableName"].Value;
            AddressRangeStart = Convert.ToUInt64(lazyIni.Sections["Settings"].Keys["addressRangeStart"].Value);
            AddressRangeEnd = Convert.ToUInt64(lazyIni.Sections["Settings"].Keys["addressRangeEnd"].Value);
            GraphicsOptionsIniLoc = lazyIni.Sections["Settings"].Keys["graphicsOptionsIniLocation"].Value;
            HUDAddressRangeStart = Convert.ToUInt64(lazyIni.Sections["Settings"].Keys["hudAddressRangeStart"].Value);
            HUDAddressRangeEnd = Convert.ToUInt64(lazyIni.Sections["Settings"].Keys["hudAddressRangeEnd"].Value);
        }

        public static void CreateLazyIni()
        {
            var resValues = GetDisplayRes();
            var lazyIni = new IniFile();
            IniSection iniVersion = lazyIni.Sections.Add("Version");
            IniSection iniResolution = lazyIni.Sections.Add("Resolution");
            IniSection iniSettings = lazyIni.Sections.Add("Settings");
            IniSection iniExperimental = lazyIni.Sections.Add("Experimental");
            iniVersion.Keys.Add("IniVersion", "2");
            iniResolution.Keys.Add("resX", Convert.ToString(resValues[0]));
            iniResolution.Keys.Add("resY", Convert.ToString(resValues[1]));
            iniSettings.Keys.Add("closeOnPatch", "true");
            iniSettings.Keys.Add("executableName", "MonsterHunterWorld");
            iniSettings.Keys.Add("addressRangeStart", "0000000000000000");
            iniSettings.Keys[2].TrailingComment.Text = "In the unlikley event of a game update breaking the mod, check the Steam/Nexus page for details/new address ranges";
            iniSettings.Keys.Add("addressRangeEnd", "1152921504606846975");
            iniSettings.Keys.Add("hudAddressRangeStart", "0000000000000000");
            iniSettings.Keys.Add("hudAddressRangeEnd", "1152921504606846975");

            if (File.Exists("C:\\Program Files(x86)\\Steam\\steamapps\\common\\Monster Hunter World\\graphics_option.ini"))
            {
                iniSettings.Keys.Add("graphicsOptionsIniLocation", "C:\\Program Files(x86)\\Steam\\steamapps\\common\\Monster Hunter World\\graphics_option.ini");
            }
            else
            {
                iniSettings.Keys.Add("graphicsOptionsIniLocation", "");
            }

            iniExperimental.Keys.Add("openGameOnPatch", "false");
            iniExperimental.TrailingComment.Text = "Options not yet enabled";

            lazyIni.Save("./lazy.ini");
        }

        public void SaveLazyIni()
        {
            var thingy = LoadIni("./lazy.ini");

            thingy.Sections["Version"].Keys["iniVersion"].Value = Convert.ToString(IniVer);
            thingy.Sections["Resolution"].Keys["resX"].Value = Convert.ToString(ResX);
            thingy.Sections["Resolution"].Keys["resY"].Value = Convert.ToString(ResY);
            thingy.Sections["Settings"].Keys["closeOnPatch"].Value = Convert.ToString(CloseOnPatch);
            thingy.Sections["Settings"].Keys["executableName"].Value = Convert.ToString(ExecutableName);
            thingy.Sections["Settings"].Keys["graphicsOptionsIniLocation"].Value = Convert.ToString(GraphicsOptionsIniLoc);
            thingy.Sections["Settings"].Keys["addressRangeStart"].Value = Convert.ToString(AddressRangeStart);
            thingy.Sections["Settings"].Keys["addressRangeEnd"].Value = Convert.ToString(AddressRangeEnd);
            thingy.Sections["Settings"].Keys["hudAddressRangeStart"].Value = Convert.ToString(HUDAddressRangeStart);
            thingy.Sections["Settings"].Keys["hudAddressRangeEnd"].Value = Convert.ToString(HUDAddressRangeEnd);
            thingy.Sections["Experimental"].Keys["openGameOnPatch"].Value = Convert.ToString(OpenGameOnPatch);            

            thingy.Save("./lazy.ini");
        }

        public static IniFile LoadIni(string path)
        {
            if (!File.Exists(path))
            {
                System.Windows.Forms.MessageBox.Show("ERROR: Could not find " + path, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                ErrorLog.Log("File not found: " + path, Environment.StackTrace);
                throw new FileNotFoundException();
            }

            var iniFile = new IniFile();
            iniFile.Load(path);
            return iniFile;
        }

        public void GetGraphicsOptionsIniLoc()
        {
            System.Windows.Forms.MessageBox.Show("Please navigate to Monster Hunter World executable.", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information);

            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = "C:\\";
                openFileDialog.Filter = "MonsterHunterWorld.exe|MonsterHunterWorld.exe";
                var response = openFileDialog.ShowDialog();

                if (response == DialogResult.Cancel || response == DialogResult.Abort)
                {
                    MonsterHunterWorldTools.ClosePatcher();
                }
                else if (response == DialogResult.OK)
                {

                    GraphicsOptionsIniLoc = openFileDialog.FileName.Replace("MonsterHunterWorld.exe", "graphics_option.ini");
                }
                else
                {
                    GetGraphicsOptionsIniLoc();
                }
            }
        }

        public static void CheckLazyIniUpdated()
        {
            if (File.Exists("./lazy.ini"))
            {
                var lazyIni = new IniFile();
                lazyIni.Load("./lazy.ini");
                if (lazyIni.Sections["Version"] == null ||
                    lazyIni.Sections["Version"].Keys["iniVersion"] == null ||
                    lazyIni.Sections["Version"].Keys["iniVersion"].Value != "2")
                {
                    File.Delete("./lazy.ini");
                }
            }
        }

        public static int[] GetDisplayRes()
        {
            int[] resValues = new int[2];

            try
            {
                resValues[0] = Convert.ToInt32(SystemParameters.PrimaryScreenWidth);
                resValues[1] = Convert.ToInt32(SystemParameters.PrimaryScreenHeight);
                return resValues;
            }

            //Because it's Windows, so it failing to know it's own screen resolution is inevitable
            catch (Exception Ex)
            {
                System.Windows.Forms.MessageBox.Show("WIDNOWS ERROR: Windows failed to report screen resolution.\n Please enter manually.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                ErrorLog.Log(Ex.ToString(), Environment.StackTrace);

                resValues[0] = 0;
                resValues[1] = 0;
                return resValues;
            }
        }
    }
}
