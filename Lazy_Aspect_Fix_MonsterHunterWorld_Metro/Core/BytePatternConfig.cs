﻿using Lazy_Aspect_Fix_MonsterHunterWorld_Metro.Helpers;

namespace Lazy_Aspect_Fix_MonsterHunterWorld_Metro
{
    public class BytePatternConfig
    {
        public byte[] Bytes;
        public ulong AddressRangeStart = 0x140000000;
        public ulong AddressRangeEnd = 0x145000000;
        public WindowsMemoryHelper.RegionPageProtection[] PageProtections;

        public BytePatternConfig(byte[] patternString, ulong addressRangeStart, ulong addressRangeEnd, params WindowsMemoryHelper.RegionPageProtection[] pageProtections)
        {
            Bytes = patternString;
            AddressRangeStart = addressRangeStart;
            AddressRangeEnd = addressRangeEnd;
            PageProtections = pageProtections;
        }
    }
}
