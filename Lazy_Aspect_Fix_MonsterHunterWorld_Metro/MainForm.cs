﻿/*
    This file is part of Lazy Aspect Fix.

    Lazy Aspect Fix is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Lazy Aspect Fix is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Lazy Aspect Fix.  If not, see <https://www.gnu.org/licenses/>. 
*/

using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;
using Lazy_Aspect_Fix_MonsterHunterWorld_Metro;
using Lazy_Aspect_Fix_MonsterHunterWorld_Metro.Core;
using Lazy_Aspect_Fix_MonsterHunterWorld_Metro.Helpers;

namespace Mace_Lazy_Aspect_fix
{
    public partial class mainForm : MetroFramework.Forms.MetroForm
    {
        public mainForm()
        {
            InitializeComponent();
            LazyIni.CheckLazyIniUpdated();

            var lazyIni = new LazyIni("./lazy.ini");
            if (string.IsNullOrWhiteSpace(lazyIni.GraphicsOptionsIniLoc) || !File.Exists(lazyIni.GraphicsOptionsIniLoc)) { lazyIni.GetGraphicsOptionsIniLoc(); }
            lazyIni.SaveLazyIni();

            userXRes.Text = Convert.ToString(Convert.ToInt32(lazyIni.ResX));
            userYRes.Text = Convert.ToString(Convert.ToInt32(lazyIni.ResY));
        }

        private async void PatchButton_Click_1(object sender, EventArgs e)
        {
            lazyProgressSpinner.Visible = true;
            lazyProgressSpinner.Spinning = true;
            patchButton.Enabled = false;

            if (!string.IsNullOrWhiteSpace(userXRes.Text) || !string.IsNullOrWhiteSpace(userYRes.Text))
            {
                    await Task.Run(() => Patch(Convert.ToInt32(userXRes.Text), Convert.ToInt32(userYRes.Text)));
            }
            else
            {
                MessageBox.Show("WARNING: Couldn't find crystal ball! (You didn't enter your resolution...)", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                patchButton.Enabled = true;
                return;
            }

            lazyProgressSpinner.Visible = false;
            lazyProgressSpinner.Spinning = false;
            patchButton.Enabled = true;
        }

        [STAThread]
        private static void Patch(int userXRes, int userYRes)
        {
            var lazyIni = new LazyIni("./lazy.ini");
            var button = new mainForm();

            if (userXRes < 400 || userXRes < 400)
            {
                MessageBox.Show("WARNING: Resolution values less than 400 are unstable. Please use a higher value.", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                button.patchButton.Enabled = true;
                return;
            }

            lazyIni.ResX = userXRes;
            lazyIni.ResY = userYRes;
            lazyIni.SaveLazyIni();

            var graphicsOptionsIni = new GraphicsOptionsIni(lazyIni.GraphicsOptionsIniLoc);

            if(graphicsOptionsIni.D3D12 == "On" || graphicsOptionsIni.DLSS == "On")
            {
                MessageBox.Show("ERROR: DirectX 12 and/or DLSS detected. These are not supported. Please disable them and restart the game.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if(graphicsOptionsIni.AspectRatio == "Off")
            {
                MessageBox.Show("ERROR: Aspect Ratio mode is set to 16:9. Please set to 21:9 in the game options.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            byte[] searchTarget = MonsterHunterWorldTools.GenerateSearchTarget(graphicsOptionsIni);
            BytePatternConfig searchTargetConfig = new BytePatternConfig(searchTarget, lazyIni.AddressRangeStart, lazyIni.AddressRangeEnd, WindowsMemoryHelper.RegionPageProtection.PAGE_READWRITE);
            BytePattern searchTargetFinal = new BytePattern(searchTargetConfig);

            Process[] gameProcess;
            try
            {
                gameProcess = GetProcess(lazyIni.ExecutableName);
                foreach (Process proc in gameProcess)
                {
                    if (proc.MainWindowTitle.Contains("MONSTER HUNTER: WORLD"))
                    {
                        gameProcess[0] = proc;
                        break;
                    }
                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show("WINDOWS ERROR: GetProcessByName critical error. " + Ex, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                ErrorLog.Log(Ex.ToString(), Environment.StackTrace);
                button.patchButton.Enabled = true;
                return;
            }

            ulong baseAddress;
            try
            {
                baseAddress = MonsterHunterWorldTools.GetAddressByteArray(gameProcess[0], searchTargetFinal, searchTargetFinal.AddressRange.Start, searchTargetFinal.AddressRange.End);
                if (baseAddress == 0)
                {
                    baseAddress = MonsterHunterWorldTools.GetAddressByteArray(gameProcess[0], searchTargetFinal, 0, 1152921504606846975);
                    if(baseAddress == 0)
                    {
                        var error = "Search target = " + searchTargetFinal.Bytes.ToString() + "graphics_option.ini | Resolution = " + graphicsOptionsIni.ResX.ToString() + "x" + graphicsOptionsIni.ResY.ToString() + " Aspect Ratio = " + graphicsOptionsIni.AspectRatio + " Address range | " + lazyIni.AddressRangeStart + " ==> " + lazyIni.AddressRangeEnd;
                        MessageBox.Show("ERROR: Failed to find resolution array. | " + error, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        ErrorLog.Log(error, Environment.StackTrace);
                        button.patchButton.Enabled = true;
                        return;
                    }
                }

                lazyIni.AddressRangeStart = Convert.ToUInt64(baseAddress - 1000000);
                lazyIni.AddressRangeEnd = Convert.ToUInt64(baseAddress + 1000000);
                lazyIni.SaveLazyIni();

                var newResolutionArrays = MonsterHunterWorldTools.GenerateNewResolutionArrays(BitConverter.GetBytes(lazyIni.ResX), BitConverter.GetBytes(lazyIni.ResY));
                try
                {
                    MonsterHunterWorldTools.WriteNewResolution(gameProcess[0], baseAddress, newResolutionArrays);
                }
                catch (Exception Ex)
                {
                    MessageBox.Show("ERROR: Failed to write to memory. " + Ex, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    ErrorLog.Log(Ex.ToString(), Environment.StackTrace);
                    button.patchButton.Enabled = true;
                    return;
                }

                byte[] hudAspectRatioSearchTargetArray = new byte[] { 0x1F, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x33, 0x33, 0x73, 0x3F, 0x00, 0x00, 0x80, 0x3E, 0x02, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00 };
                BytePatternConfig hudAspectRatioSearchConfig = new BytePatternConfig(hudAspectRatioSearchTargetArray, lazyIni.HUDAddressRangeStart, lazyIni.HUDAddressRangeEnd, WindowsMemoryHelper.RegionPageProtection.PAGE_READWRITE);
                BytePattern hudAspectRatioSearchFinal = new BytePattern(hudAspectRatioSearchConfig);

                baseAddress = MonsterHunterWorldTools.GetAddressByteArray(gameProcess[0], hudAspectRatioSearchFinal, hudAspectRatioSearchFinal.AddressRange.Start, hudAspectRatioSearchFinal.AddressRange.End);
                if(baseAddress == 0)
                {
                    baseAddress = MonsterHunterWorldTools.GetAddressByteArray(gameProcess[0], hudAspectRatioSearchFinal, 0, 1152921504606846975);
                    if (baseAddress == 0)
                    {
                        var error = "Search target = " + searchTargetFinal.Bytes.ToString() + "graphics_option.ini | Resolution = " + graphicsOptionsIni.ResX.ToString() + "x" + graphicsOptionsIni.ResY.ToString() + " Aspect Ratio = " + graphicsOptionsIni.AspectRatio + " Address range | " + lazyIni.AddressRangeStart + " ==> " + lazyIni.AddressRangeEnd;
                        MessageBox.Show("ERROR: Failed to find resolution array. | " + error, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        ErrorLog.Log(error, Environment.StackTrace);
                        button.patchButton.Enabled = true;
                        return;
                    }
                }

                lazyIni.HUDAddressRangeStart = Convert.ToUInt64(baseAddress - 1000000);
                lazyIni.HUDAddressRangeEnd = Convert.ToUInt64(baseAddress + 1000000);
                lazyIni.SaveLazyIni();

                var newHudAspect = (float)userXRes / userYRes;
                byte[] newHudAspectRatio = BitConverter.GetBytes(newHudAspect);

                try
                {
                    MonsterHunterWorldTools.WriteSingleArray(gameProcess[0], baseAddress + 40, newHudAspectRatio);
                }
                catch (Exception Ex)
                {
                    MessageBox.Show("ERROR: Failed to write to memory. " + Ex, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    ErrorLog.Log(Ex.ToString(), Environment.StackTrace);
                    button.patchButton.Enabled = true;
                    return;
                }

                baseAddress += 983178308;
                byte[] newUserMasterOffset = MonsterHunterWorldTools.GenerateUserMasterOffset(userYRes / userYRes, graphicsOptionsIni.UltraWideModeLayout / 100);
                var userSliderValue = BitConverter.GetBytes(graphicsOptionsIni.UltraWideModeLayout / 100);
                try
                {
                    MonsterHunterWorldTools.WriteSingleArray(gameProcess[0], baseAddress, new byte[] { 0x00, 0x00, 0x00, 0x00 });
                    MonsterHunterWorldTools.WriteSingleArray(gameProcess[0], baseAddress + 4, new byte[] { 0x00, 0x00, 0x00, 0x00 });
                    MonsterHunterWorldTools.WriteSingleArray(gameProcess[0], baseAddress + 8, new byte[] { 0x00, 0x00, 0x00, 0x00 });

                    MonsterHunterWorldTools.WriteSingleArray(gameProcess[0], baseAddress, userSliderValue);
                    MonsterHunterWorldTools.WriteSingleArray(gameProcess[0], baseAddress + 4, newUserMasterOffset);
                    MonsterHunterWorldTools.WriteSingleArray(gameProcess[0], baseAddress + 8, newUserMasterOffset);
                }
                catch (Exception Ex)
                {
                    MessageBox.Show("ERROR: Failed to write to memory. " + Ex, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    ErrorLog.Log(Ex.ToString(), Environment.StackTrace);
                    button.patchButton.Enabled = true;
                    return;
                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show("ERROR: Testing/finding bas address. " + Ex, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                ErrorLog.Log(Ex.ToString(), Environment.StackTrace);
                button.patchButton.Enabled = true;
                return;
            }

            if (lazyIni.CloseOnPatch == true)
            {
                MonsterHunterWorldTools.ClosePatcher();
            }
        }

        private static Process[] GetProcess(string executableName)
        {
            var process = Process.GetProcessesByName(executableName);
            if (process.Length < 1) { throw new Exception("Failed to find executable. Process not running."); }
            return (process);
        }
    }
}
